<?php
namespace In2code\Build;

use Composer\Script\Event;
use Version\Version;

/**
 * TODO: Rename this class to have a more appropriate name and use a proxy to not break backwards compatibility!
 */
class Composer
{
    const PROJECT_TYPE_TYPO3_CMS_PROJECT = 1;
    const PROJECT_TYPE_TYPO3_CMS_EXTENSION = 2;
    const PROJECT_TYPE_PROJECT = 3;

    const COMPOSER_PACKAGE_TYPE_TYPO3_CMS_EXTENSION = 'typo3-cms-extension';
    const BUILDFILE_NAME = 'build.xml';

    const PLACEHOLDER_IN_BUILDFILE_PATH_COMPOSER_PROJECT = '### path-composer-project ###';
    const PLACEHOLDER_IN_BUILDFILE_PATH_COMPOSER_VENDOR_DIRECTORY= '### path-composer-vendor-directory ###';
    const PLACEHOLDER_IN_BUILDFILE_PATH_WEB_DIRECTORY = '### path-web-directory ###';
    const PLACEHOLDER_IN_BUILDFILE_PATH_ROOT_DIRECTORY_FOR_TESTING = '### path-root-directory-for-testing ###';
    const PLACEHOLDER_IN_BUILDFILE_PATH_BUILD_DIRECTORY_RELATIVE = '### path-build-directory-relative ###';
    const PLACEHOLDER_IN_BUILDFILE_PATHS_INCLUDE_ELEMENTS = '<!-- ### paths-include-elements ### -->';
    const PLACEHOLDER_IN_BUILDFILE_PATHS_ARG_ELEMENTS = '<!-- ### paths-arg-elements ### -->';
    const PLACEHOLDER_IN_BUILDFILE_EXCEPT_VALUE_ARG_ELEMENTS = '<!-- ### except-value-arg-elements ### -->';
    const PLACEHOLDER_IN_BUILDFILE_PATHS_COMMASEPARATED_PATHS = '### paths-commaseparated ###';
    const PLACEHOLDER_IN_BUILDFILE_PROJECT_SPECIFIC_EXTENSIONS_ARE_PRESENT = '### project-specific-extensions-are-present ###';
    const PLACEHOLDER_IN_BUILDFILE_PROJECT_TYPE = '### project-type ###';
    const PLACEHOLDER_IN_BUILDFILE_STRING_001 = '### string-001 ###';
    const PLACEHOLDER_IN_BUILDFILE_STRING_002 = '### string-002 ###';
    const PLACEHOLDER_IN_BUILDFILE_STRING_003 = '### string-003 ###';
    const PLACEHOLDER_IN_PHPDOX_XML_FILE_PATHS_INCLUDE_ELEMENTS = '<!-- ### include-elements ### -->';
    const PLACEHOLDER_IN_PHPDOX_XML_FILE_PATHS_PROJECT_SOURCE = '### project-source ###';
    const PLACEHOLDER_IN_PHPDOX_XML_FILE_PATHS_BUILD_DIRECTORY = '### path-build-directory ###';
    const PLACEHOLDER_IN_PHPDOX_XML_PROJECT_NAME = '### project-name ###';
    const PLACEHOLDER_IN_PHPUNIT_UNITTESTS_XML_FILE_PATH_BOOTSTRAP_FILE = '### path-bootstrap-file ###';
    const PLACEHOLDER_IN_PHPUNIT_UNITTESTS_XML_FILE_DIRECTORY_ELEMENTS_TESTSUITE_ALL_TESTS = '<!-- ### directory-elements-testsuite-all-tests ### -->';
    const PLACEHOLDER_IN_PHPUNIT_UNITTESTS_XML_FILE_DIRECTORY_ELEMENTS_WHITELIST = '<!-- ### directory-elements-whitelist ### -->';
    const PLACEHOLDER_IN_PHPUNIT_UNITTESTS_XML_FILE_DIRECTORY_ELEMENTS_WHITELIST_EXCLUDE = '<!-- ### directory-elements-whitelist-exclude ### -->';
    const PLACEHOLDER_IN_PHPUNIT_FUNCTIONALTESTS_XML_FILE_PATH_BOOTSTRAP_FILE = '### path-bootstrap-file ###';
    const PLACEHOLDER_IN_PHPUNIT_FUNCTIONALTESTS_XML_FILE_DIRECTORY_ELEMENTS_TESTSUITE_ALL_TESTS = '<!-- ### directory-elements-testsuite-all-tests ### -->';
    const PLACEHOLDER_IN_PHPUNIT_FUNCTIONALTESTS_XML_FILE_DIRECTORY_ELEMENTS_WHITELIST = '<!-- ### directory-elements-whitelist ### -->';
    const PLACEHOLDER_IN_PHPUNIT_FUNCTIONALTESTS_XML_FILE_DIRECTORY_ELEMENTS_WHITELIST_EXCLUDE = '<!-- ### directory-elements-whitelist-exclude ### -->';
    const PLACEHOLDER_IN_PHPCSFIXER_CONFIGURATION_FILE_PATHS_COMMASEPARATED_PATHS = '### paths-commaseparated ###';

    /**
     * @var Event
     */
    protected $composerScriptEvent;

    /**
     * @param Event $event
     * @return void
     */
    public static function postInstall(Event $event)
    {
        $composer = new static($event);
        $composer->prepareConfigurationFiles();
    }

    /**
     * @param Event $event
     * @return void
     */
    public static function postUpdate(Event $event)
    {
        $composer = new static($event);
        $composer->prepareConfigurationFiles();
    }

    /**
     * @param Event $event
     * @return void
     */
    public static function prepare(Event $event)
    {
        $composer = new static($event);
        $composer->prepareConfigurationFiles();
    }

    /**
     * @param Event $composerEvent
     */
    public function __construct(Event $composerEvent)
    {
        $this->composerScriptEvent = $composerEvent;

        $this->composerScriptEvent->getIO()->write(chr(10) . '<warning>IMPORTANT: You have to require \'typo3/testing-framework\' package in your root composer.json file if you use TYPO3 CMS 8.7 LTS! Run \'composer require --dev typo3/testing-framework\' to do so.</warning>' . chr(10));
    }

    /**
     * @return void
     */
    protected function prepareConfigurationFiles()
    {
        $this->copyEditorconfigFileToProjectRoot();

        $this->copyBuildfileToProjectRoot();
        $this->replacePlaceholdersInBuildfile();

        $this->copyPhpdoxXmlFile();
        $this->replacePlaceholdersInPhpdoxXmlFile();

        $this->copyPhpunitUnittestsXmlFile();
        $this->replacePlaceholdersInPhpunitUnittestsXmlFile();

        $this->copyPhpunitFunctionaltestsXmlFile();
        $this->replacePlaceholdersInPhpunitFunctionaltestsXmlFile();

        $this->copyPhpcsfixerConfigurationFile();
        $this->replacePlaceholdersInPhpcsfixerConfigurationFile();

        $this->ensureThatPackageJsonFileExists();
    }

    /**
     * @return void
     */
    protected function copyEditorconfigFileToProjectRoot()
    {
        copy($this->getPathEditorconfigSourceFile(), $this->getPathEditorconfigTargetFile());
    }

    /**
     * @return void
     */
    protected function copyPhpdoxXmlFile()
    {
        copy($this->getPathPhpdoxXmlFileWithPlaceholders(), $this->getPathPhpdoxXmlFile());
    }

    /**
     * @return void
     */
    protected function copyBuildfileToProjectRoot()
    {
        copy($this->getPathBuildfileWithPlaceholders(), $this->getPathBuildfile());
    }

    /**
     * @return void
     */
    protected function copyPhpunitUnittestsXmlFile()
    {
        copy($this->getPathPhpunitUnittestsXmlFileWithPlaceholders(), $this->getPathPhpunitUnittestsXmlFile());
    }

    /**
     * @return void
     */
    protected function copyPhpunitFunctionaltestsXmlFile()
    {
        copy($this->getPathPhpunitFunctionaltestsXmlFileWithPlaceholders(), $this->getPathPhpunitFunctionaltestsXmlFile());
    }

    /**
     * @return void
     */
    protected function copyPhpcsfixerConfigurationFile()
    {
        copy($this->getPathPhpcsfixerConfigurationFileWithPlaceholders(), $this->getPathPhpcsfixerConfigurationFile());
    }

    /**
     * @return void
     */
    protected function replacePlaceholdersInBuildfile()
    {
        $this->replacePathPlaceholdersInBuildfileProjectType();
        $this->replaceProjectSpecificExtensionsArePresentPlaceholder();
        $this->replacePathPlaceholdersInBuildfilePathArgElements();
        $this->replacePathPlaceholdersInBuildfileExceptValueArgElements();
        $this->replacePathPlaceholdersInBuildfileCommaseparatedPaths();
        $this->replacePathPlaceholdersInBuildfileIncludeElements();
        $this->replacePathPlaceholdersInBuildfileWebDirectory();
        $this->replacePathPlaceholdersInBuildfileRootDirectoryForTesting();
        $this->replacePathPlaceholdersInBuildfileBuildDirectoryRelative();
        $this->replacePathPlaceholdersInBuildfileComposerProject();
        $this->replacePathPlaceholdersInBuildfileComposerVendorDirectory();
        $this->replacePathPlaceholdersInBuildfileVariousStrings();
    }

    /**
     * @return void
     */
    protected function replacePlaceholdersInPhpdoxXmlFile()
    {
        $this->replacePathPlaceholdersInPhpdoxXmlFileCollectorIncludeElements();
        $this->replacePathPlaceholdersInPhpdoxXmlFileProjectSource();
        $this->replacePathPlaceholdersInPhpdoxXmlFileBuildDirectory();
        $this->replaceProjectNamePlaceholderInPhpdoxXmlFile();
    }

    /**
     * @return void
     */
    protected function replacePlaceholdersInPhpunitUnittestsXmlFile()
    {
        $this->replacePathPlaceholdersInPhpunitUnittestsXmlFilePathBootstrapFile();
        $this->replacePathPlaceholdersInPhpunitUnittestsXmlFileDirectoryElementsTestsuiteAllTests();
        $this->replacePathPlaceholdersInPhpunitUnittestsXmlFileDirectoryElementsWhitelist();
        $this->replacePathPlaceholdersInPhpunitUnittestsXmlFileDirectoryElementsWhitelistExcludes();
    }

    /**
     * @return void
     */
    protected function replacePlaceholdersInPhpunitFunctionaltestsXmlFile()
    {
        $this->replacePathPlaceholdersInPhpunitFunctionaltestsXmlFilePathBootstrapFile();
        $this->replacePathPlaceholdersInPhpunitFunctionaltestsXmlFileDirectoryElementsTestsuiteAllTests();
        $this->replacePathPlaceholdersInPhpunitFunctionaltestsXmlFileDirectoryElementsWhitelist();
    }

    /**
     * @return void
     */
    protected function replacePlaceholdersInPhpcsfixerConfigurationFile()
    {
        $this->replacePathPlaceholdersInPhpcsfixerConfigurationFileCommaseparatedPaths();
    }

    /**
     * @return void
     */
    protected function ensureThatPackageJsonFileExists()
    {
        $pathPackageJsonFile = $this->getPathPackageJsonFile();
        if (!file_exists($pathPackageJsonFile)) {
            copy($this->getPathPackageJsonFileFallback(), $pathPackageJsonFile);
        }
    }

    /**
     * @return array
     */
    protected function getPackageNamesOfTypo3ExtensionsInstalledViaComposer()
    {
        $result = [];

        $packages = $this->composerScriptEvent
            ->getComposer()
            ->getRepositoryManager()
            ->getLocalRepository()
            ->getPackages();

        foreach ($packages as $package) {
            /** @var \Composer\Package\CompletePackage $package */
            if (
                $package->getType() === self::COMPOSER_PACKAGE_TYPE_TYPO3_CMS_EXTENSION
            ) {
                $result[] = $package->getName();
                foreach ($package->getReplaces() as $replacedPackage) {
                    $target = $replacedPackage->getTarget();
                    if (strpos($target, '/')) {
                        $result[] = $target;
                    }
                }
            }
        }

        return array_unique($result);
    }

    /**
     * @param array $typo3ExtensionsPackageNames
     * @return array
     */
    protected function getExtensionNamesFromPackageNames($typo3ExtensionsPackageNames)
    {
        return array_unique(
            array_map(function ($packageName) {
                return $this->getExtensionNameFromPackageNameWithoutVendorName(
                    $this->getPackageNameWithoutVendorName($packageName)

                );
            }, $typo3ExtensionsPackageNames)
        );
    }

    /**
     * @param string $packageName
     * @return string
     */
    protected function getPackageNameWithoutVendorName($packageName)
    {
        return substr(
            $packageName,
            strpos($packageName, '/') + 1
        );
    }

    /**
     * @param string $packageNameWithoutVendorName
     * @return string
     */
    protected function getExtensionNameFromPackageNameWithoutVendorName($packageNameWithoutVendorName)
    {
        return str_replace('-', '_', $packageNameWithoutVendorName);
    }

    /**
     * @return array
     */
    protected function getAllExtensionFolderNames()
    {
        return array_diff(
            scandir($this->getPathTypo3ExtensionsDirectory()),
            ['.', '..', '.DS_Store']
        );
    }

    /**
     * @return string
     */
    protected function replacePathPlaceholdersInBuildfileProjectType()
    {
        if ($this->composerRootPackageIsProject()) {
            $replacement = self::PROJECT_TYPE_PROJECT;
        } elseif ($this->composerRootPackageIsTypo3CmsExtension()) {
            $replacement = self::PROJECT_TYPE_TYPO3_CMS_EXTENSION;
        } else {
            $replacement = self::PROJECT_TYPE_TYPO3_CMS_PROJECT;
        }

        file_put_contents(
            $this->getPathBuildfile(),
            str_replace(
                self::PLACEHOLDER_IN_BUILDFILE_PROJECT_TYPE,
                $replacement,
                file_get_contents($this->getPathBuildfile())
            )
        );
    }

    /**
     * TODO: rename "project-specific-extensions-are-present" to a more generic term because we now also support non TYPO3 projects!
     *
     * @return void
     */
    protected function replaceProjectSpecificExtensionsArePresentPlaceholder()
    {
        if ($this->composerRootPackageIsProject()) {
            $replacement = 'true';
        } elseif (0 < count($this->getProjectSpecificExtensionFolderNames())) {
            $replacement = 'true';
        } else {
            $replacement = 'false';
        }

        file_put_contents(
            $this->getPathBuildfile(),
            str_replace(
                self::PLACEHOLDER_IN_BUILDFILE_PROJECT_SPECIFIC_EXTENSIONS_ARE_PRESENT,
                $replacement,
                file_get_contents($this->getPathBuildfile())
            )
        );
    }

    /**
     * @return void
     */
    protected function replacePathPlaceholdersInBuildfilePathArgElements()
    {
        $replacement = [];

        if ($this->composerRootPackageIsProject()) {
            $replacement[] = '<arg path="${path-root-directory-for-testing}" />';
        } elseif ($this->composerRootPackageIsTypo3CmsExtension()) {
            $replacement[] = '<arg path="${path-root-directory-for-testing}" />';
        } else {
            foreach ($this->getProjectSpecificExtensionFolderNames() as $folderName) {
                $replacement[] = sprintf(
                    '<arg path="${path-root-directory-for-testing}/%s" />',
                    $folderName
                );
            }
        }

        file_put_contents(
            $this->getPathBuildfile(),
            str_replace(
                self::PLACEHOLDER_IN_BUILDFILE_PATHS_ARG_ELEMENTS,
                implode('', $replacement),
                file_get_contents($this->getPathBuildfile())
            )
        );
    }

    /**
     * @return void
     */
    protected function replacePathPlaceholdersInBuildfileExceptValueArgElements()
    {
        $replacement = [];
        foreach ($this->getNotProjectSpecificExtensionFolderNames() as $folderName) {
            $replacement[] = sprintf(
                '<arg value="--except=${path-root-directory-for-testing}/%s" />',
                $folderName
            );
        }

        file_put_contents(
            $this->getPathBuildfile(),
            str_replace(
                self::PLACEHOLDER_IN_BUILDFILE_EXCEPT_VALUE_ARG_ELEMENTS,
                implode('', $replacement),
                file_get_contents($this->getPathBuildfile())
            )
        );
    }

    /**
     * @return void
     */
    protected function replacePathPlaceholdersInBuildfileCommaseparatedPaths()
    {
        $paths = [];

        if ($this->composerRootPackageIsProject()) {
            $paths[] = '${path-root-directory-for-testing}';
        } elseif ($this->composerRootPackageIsTypo3CmsExtension()) {
            $paths[] = '${path-root-directory-for-testing}';
        } else {
            foreach ($this->getProjectSpecificExtensionFolderNames() as $folderName) {
                $paths[] = sprintf(
                    '${path-root-directory-for-testing}/%s',
                    $folderName
                );
            }
        }

        $replacement = implode(',', $paths);

        file_put_contents(
            $this->getPathBuildfile(),
            str_replace(
                self::PLACEHOLDER_IN_BUILDFILE_PATHS_COMMASEPARATED_PATHS,
                $replacement,
                file_get_contents($this->getPathBuildfile())
            )
        );
    }

    /**
     * @return void
     */
    protected function replacePathPlaceholdersInBuildfileIncludeElements()
    {
        $replacement = [];

        if ($this->composerRootPackageIsProject()) {
            $replacement[] = '<include name="**/*.php" />';
            $replacement[] = '<exclude name="Build/**" />';
        } elseif ($this->composerRootPackageIsTypo3CmsExtension()) {
            $replacement[] = '<include name="**/*.php" />';
            $replacement[] = '<exclude name=".Build/**" />';
        } else {
            foreach ($this->getProjectSpecificExtensionFolderNames() as $folderName) {
                $replacement[] = sprintf(
                    '<include name="%s/**/*.php" />',
                    $folderName
                );
            }
        }

        file_put_contents(
            $this->getPathBuildfile(),
            str_replace(
                self::PLACEHOLDER_IN_BUILDFILE_PATHS_INCLUDE_ELEMENTS,
                implode('', $replacement),
                file_get_contents($this->getPathBuildfile())
            )
        );
    }

    /**
     * TODO: rename "project-specific-extensions-are-present" to a more generic term because we now also support non TYPO3 projects!
     *
     * @return void
     */
    protected function replacePathPlaceholdersInBuildfileWebDirectory()
    {
        if ($this->composerRootPackageIsProject()) {
            $replacement = '${basedir}';
        } elseif ($this->composerRootPackageIsTypo3CmsExtension()) {
            $replacement = '${basedir}/.Build/Web';
        } else {
            $replacement = '${basedir}/Web';
        }

        file_put_contents(
            $this->getPathBuildfile(),
            str_replace(
                self::PLACEHOLDER_IN_BUILDFILE_PATH_WEB_DIRECTORY,
                $replacement,
                file_get_contents($this->getPathBuildfile())
            )
        );
    }

    /**
     * @return void
     */
    protected function replacePathPlaceholdersInBuildfileRootDirectoryForTesting()
    {
        if ($this->composerRootPackageIsProject()) {
            $replacement = '${path-build-directory-absolute}/WorkingDirectoryForTools/CopyOfProject/src';
        } elseif ($this->composerRootPackageIsTypo3CmsExtension()) {
            $replacement = '${path-build-directory-absolute}/WorkingDirectoryForTools/CopyOfProject';
        } else {
            $replacement = '${basedir}/Web/typo3conf/ext';
        }

        file_put_contents(
            $this->getPathBuildfile(),
            str_replace(
                self::PLACEHOLDER_IN_BUILDFILE_PATH_ROOT_DIRECTORY_FOR_TESTING,
                $replacement,
                file_get_contents($this->getPathBuildfile())
            )
        );
    }

    /**
     * @return void
     */
    protected function replacePathPlaceholdersInBuildfileBuildDirectoryRelative()
    {
        if ($this->composerRootPackageIsProject()) {
            $replacement = 'Build';
        } elseif ($this->composerRootPackageIsTypo3CmsExtension()) {
            $replacement = '.Build/Build';
        } else {
            $replacement = 'Build';
        }

        file_put_contents(
            $this->getPathBuildfile(),
            str_replace(
                self::PLACEHOLDER_IN_BUILDFILE_PATH_BUILD_DIRECTORY_RELATIVE,
                $replacement,
                file_get_contents($this->getPathBuildfile())
            )
        );
    }

    /**
     * @return void
     */
    protected function replacePathPlaceholdersInBuildfileComposerProject()
    {
        if ($this->composerRootPackageIsProject()) {
            $replacement = '.';
        } elseif ($this->composerRootPackageIsTypo3CmsExtension()) {
            $replacement = '.';
        } else {
            $replacement = './Web';
        }

        file_put_contents(
            $this->getPathBuildfile(),
            str_replace(
                self::PLACEHOLDER_IN_BUILDFILE_PATH_COMPOSER_PROJECT,
                $replacement,
                file_get_contents($this->getPathBuildfile())
            )
        );
    }

    /**
     * @return void
     */
    protected function replacePathPlaceholdersInBuildfileComposerVendorDirectory()
    {
        if ($this->composerRootPackageIsProject()) {
            $replacement = 'vendor/bin/';
        } elseif ($this->composerRootPackageIsTypo3CmsExtension()) {
            $replacement = '../bin/';
        } else {
            $replacement = 'vendor/bin/';
        }

        file_put_contents(
            $this->getPathBuildfile(),
            str_replace(
                self::PLACEHOLDER_IN_BUILDFILE_PATH_COMPOSER_VENDOR_DIRECTORY,
                $replacement,
                file_get_contents($this->getPathBuildfile())
            )
        );
    }

    /**
     * @return void
     */
    protected function replacePathPlaceholdersInBuildfileVariousStrings()
    {
        if ($this->composerRootPackageIsProject()) {
            $replacements = array(
                '001' => '',
                '002' => '',
                '003' => '',
            );
        } elseif ($this->composerRootPackageIsTypo3CmsExtension()) {
            $replacements = array(
                '001' => '!!!!!!!',
                '002' => '.Build/',
                '003' => '       ',
            );
        } else {
            $replacements = array(
                '001' => '',
                '002' => '',
                '003' => '',
            );
        }

        foreach ($replacements as $replacementKey => $replacement) {
            file_put_contents(
                $this->getPathBuildfile(),
                str_replace(
                    constant('self::PLACEHOLDER_IN_BUILDFILE_STRING_' . $replacementKey),
                    $replacement,
                    file_get_contents($this->getPathBuildfile())
                )
            );
        }
    }

    /**
     * @return void
     */
    protected function replacePathPlaceholdersInPhpdoxXmlFileCollectorIncludeElements()
    {
        $replacement = [];

        if ($this->composerRootPackageIsProject()) {
            $replacement[] = '<include mask="src/**/*.php" />';
        } elseif ($this->composerRootPackageIsTypo3CmsExtension()) {
            $replacement[] = '<include mask=".Build/Build/WorkingDirectoryForTools/CopyOfProject/**/*.php" />';
        } else {
            foreach ($this->getProjectSpecificExtensionFolderNames() as $folderName) {
                $replacement[] = sprintf(
                    '<include mask="Web/typo3conf/ext/%s/**/*.php" />',
                    $folderName
                );
            }
        }

        file_put_contents(
            $this->getPathPhpdoxXmlFile(),
            str_replace(
                self::PLACEHOLDER_IN_PHPDOX_XML_FILE_PATHS_INCLUDE_ELEMENTS,
                implode('', $replacement),
                file_get_contents($this->getPathPhpdoxXmlFile())
            )
        );
    }

    /**
     * @return void
     */
    protected function replacePathPlaceholdersInPhpdoxXmlFileProjectSource()
    {
        if ($this->composerRootPackageIsProject()) {
            $replacement = 'src';
        } elseif ($this->composerRootPackageIsTypo3CmsExtension()) {
            $replacement = '.Build/Build/WorkingDirectoryForTools/CopyOfProject/';
        } else {
            $replacement = 'Web/typo3conf/ext';
        }

        file_put_contents(
            $this->getPathPhpdoxXmlFile(),
            str_replace(
                self::PLACEHOLDER_IN_PHPDOX_XML_FILE_PATHS_PROJECT_SOURCE,
                $replacement,
                file_get_contents($this->getPathPhpdoxXmlFile())
            )
        );
    }

    /**
     * @return void
     */
    protected function replacePathPlaceholdersInPhpdoxXmlFileBuildDirectory()
    {
        if ($this->composerRootPackageIsProject()) {
            $replacement = 'Build';
        } elseif ($this->composerRootPackageIsTypo3CmsExtension()) {
            $replacement = '.Build/Build';
        } else {
            $replacement = 'Build';
        }

        file_put_contents(
            $this->getPathPhpdoxXmlFile(),
            str_replace(
                self::PLACEHOLDER_IN_PHPDOX_XML_FILE_PATHS_BUILD_DIRECTORY,
                $replacement,
                file_get_contents($this->getPathPhpdoxXmlFile())
            )
        );
    }

    /**
     * @return void
     */
    protected function replaceProjectNamePlaceholderInPhpdoxXmlFile()
    {
        file_put_contents(
            $this->getPathPhpdoxXmlFile(),
            str_replace(
                self::PLACEHOLDER_IN_PHPDOX_XML_PROJECT_NAME,
                $this->composerScriptEvent
                    ->getComposer()
                    ->getPackage()
                    ->getName(),
                file_get_contents($this->getPathPhpdoxXmlFile())
            )
        );
    }

    /**
     * @return void
     */
    protected function replacePathPlaceholdersInPhpunitUnittestsXmlFilePathBootstrapFile()
    {
        $replacement = '';

        if ($this->composerRootPackageIsProject()) {
            $replacement = '../../../vendor/autoload.php';
        } else {
            if ($this->weUseTheExternalTestingFrameworkProvidedByTheTypo3CoreTeam()) {
                $replacement = '../../../Web/vendor/typo3/testing-framework/Resources/Core/Build/UnitTestsBootstrap.php';
            } elseif ($this->weUseTheInternalTestingFrameworkProvidedByTheTypo3CoreTeam()) {
                $replacement = '../../../Web/typo3_src/components/testing_framework/Resources/Core/Build/UnitTestsBootstrap.php';
            } else {
                $replacement = '../../../Web/typo3/sysext/core/Build/UnitTestsBootstrap.php';
            }
        }

        file_put_contents(
            $this->getPathPhpunitUnittestsXmlFile(),
            str_replace(
                self::PLACEHOLDER_IN_PHPUNIT_UNITTESTS_XML_FILE_PATH_BOOTSTRAP_FILE,
                $replacement,
                file_get_contents($this->getPathPhpunitUnittestsXmlFile())
            )
        );
    }

    /**
     * @return void
     */
    protected function replacePathPlaceholdersInPhpunitUnittestsXmlFileDirectoryElementsTestsuiteAllTests()
    {
        $replacement = [];

        if ($this->composerRootPackageIsProject()) {
            $replacement[] = '<directory>../../../Tests/Unit</directory>';
        } elseif ($this->composerRootPackageIsTypo3CmsExtension()) {
            $replacement[] = '<directory>../../../../Tests/Unit</directory>';
        } else {
            foreach ($this->getProjectSpecificExtensionFolderNames() as $folderName) {
                $replacement[] = sprintf(
                    '<directory>../../../Web/typo3conf/ext/%s/Tests/Unit</directory>',
                    $folderName
                );
            }
        }

        file_put_contents(
            $this->getPathPhpunitUnittestsXmlFile(),
            str_replace(
                self::PLACEHOLDER_IN_PHPUNIT_UNITTESTS_XML_FILE_DIRECTORY_ELEMENTS_TESTSUITE_ALL_TESTS,
                implode('', $replacement),
                file_get_contents($this->getPathPhpunitUnittestsXmlFile())
            )
        );
    }

    /**
     * @return void
     */
    protected function replacePathPlaceholdersInPhpunitUnittestsXmlFileDirectoryElementsWhitelist()
    {
        $replacement = [];

        if ($this->composerRootPackageIsProject()) {
            $replacement[] = '<directory suffix=".php">../../../src</directory>';
        } elseif ($this->composerRootPackageIsTypo3CmsExtension()) {
            $replacement[] = '<directory suffix=".php">../../../../Classes</directory>';
        } else {
            foreach ($this->getProjectSpecificExtensionFolderNames() as $folderName) {
                $replacement[] = sprintf(
                    '<directory suffix=".php">../../../Web/typo3conf/ext/%s/Classes</directory><directory suffix=".php">../../../Web/typo3conf/ext/%s/pi*</directory>',
                    $folderName,
                    $folderName
                );
            }
        }

        file_put_contents(
            $this->getPathPhpunitUnittestsXmlFile(),
            str_replace(
                self::PLACEHOLDER_IN_PHPUNIT_UNITTESTS_XML_FILE_DIRECTORY_ELEMENTS_WHITELIST,
                implode('', $replacement),
                file_get_contents($this->getPathPhpunitUnittestsXmlFile())
            )
        );
    }

    /**
     * @return void
     */
    protected function replacePathPlaceholdersInPhpunitUnittestsXmlFileDirectoryElementsWhitelistExcludes()
    {
        $replacement = [];

        if ($this->composerRootPackageIsProject()) {
            $replacement = [];
        } elseif ($this->composerRootPackageIsTypo3CmsExtension()) {
            $replacement[] = '<directory suffix=".php">../../../../Classes/Controller</directory><directory suffix=".php">../../../../Classes/Domain/Repository</directory>';
        } else {
            foreach ($this->getProjectSpecificExtensionFolderNames() as $folderName) {
                $replacement[] = sprintf(
                    '<directory suffix=".php">../../../Web/typo3conf/ext/%s/Classes/Controller</directory><directory suffix=".php">../../../Web/typo3conf/ext/%s/Classes/Domain/Repository</directory><directory suffix=".php">../../../Web/typo3conf/ext/%s/pi*/**/lib</directory>',
                    $folderName,
                    $folderName,
                    $folderName
                );
            }
        }

        file_put_contents(
            $this->getPathPhpunitUnittestsXmlFile(),
            str_replace(
                self::PLACEHOLDER_IN_PHPUNIT_UNITTESTS_XML_FILE_DIRECTORY_ELEMENTS_WHITELIST_EXCLUDE,
                implode('', $replacement),
                file_get_contents($this->getPathPhpunitUnittestsXmlFile())
            )
        );
    }

    /**
     * @return void
     */
    protected function replacePathPlaceholdersInPhpunitFunctionaltestsXmlFilePathBootstrapFile()
    {
        $replacement = '';

        if ($this->composerRootPackageIsProject()) {
            $replacement = '../../../vendor/autoload.php';
        } else {
            if ($this->weUseTheExternalTestingFrameworkProvidedByTheTypo3CoreTeam()) {
                $replacement = '../../../Web/vendor/typo3/testing-framework/Resources/Core/Build/FunctionalTestsBootstrap.php';
            } elseif ($this->weUseTheInternalTestingFrameworkProvidedByTheTypo3CoreTeam()) {
                $replacement = '../../../Web/typo3_src/components/testing_framework/Resources/Core/Build/FunctionalTestsBootstrap.php';
            } else {
                $replacement = '../../../Web/typo3/sysext/core/Build/FunctionalTestsBootstrap.php';
            }
        }

        file_put_contents(
            $this->getPathPhpunitFunctionaltestsXmlFile(),
            str_replace(
                self::PLACEHOLDER_IN_PHPUNIT_FUNCTIONALTESTS_XML_FILE_PATH_BOOTSTRAP_FILE,
                $replacement,
                file_get_contents($this->getPathPhpunitFunctionaltestsXmlFile())
            )
        );
    }

    /**
     * @return void
     */
    protected function replacePathPlaceholdersInPhpunitFunctionaltestsXmlFileDirectoryElementsTestsuiteAllTests()
    {
        $replacement = [];

        if ($this->composerRootPackageIsProject()) {
            $replacement[] = '<directory>../../../Tests/Functional</directory>';
        } elseif ($this->composerRootPackageIsTypo3CmsExtension()) {
            $replacement[] = '<directory>../../../../Tests/Functional</directory>';
        } else {
            foreach ($this->getProjectSpecificExtensionFolderNames() as $folderName) {
                $replacement[] = sprintf(
                    '<directory>../../../Web/typo3conf/ext/%s/Tests/Functional</directory>',
                    $folderName
                );
            }
        }

        file_put_contents(
            $this->getPathPhpunitFunctionaltestsXmlFile(),
            str_replace(
                self::PLACEHOLDER_IN_PHPUNIT_FUNCTIONALTESTS_XML_FILE_DIRECTORY_ELEMENTS_TESTSUITE_ALL_TESTS,
                implode('', $replacement),
                file_get_contents($this->getPathPhpunitFunctionaltestsXmlFile())
            )
        );
    }

    /**
     * @return void
     */
    protected function replacePathPlaceholdersInPhpunitFunctionaltestsXmlFileDirectoryElementsWhitelist()
    {
        $replacement = [];

        if ($this->composerRootPackageIsProject()) {
            $replacement[] = '<directory suffix=".php">../../../src</directory>';
        } elseif ($this->composerRootPackageIsTypo3CmsExtension()) {
            $replacement[] = '<directory suffix=".php">../../../../Classes/Controller</directory>';
            $replacement[] = '<directory suffix=".php">../../../../Classes/Repository</directory>';
        } else {
            foreach ($this->getProjectSpecificExtensionFolderNames() as $folderName) {
                $replacement[] = sprintf(
                    '<directory suffix=".php">../../../Web/typo3conf/ext/%s/Classes/Controller</directory>',
                    $folderName
                );
                $replacement[] = sprintf(
                    '<directory suffix=".php">../../../Web/typo3conf/ext/%s/Classes/Repository</directory>',
                    $folderName
                );
            }
        }

        file_put_contents(
            $this->getPathPhpunitFunctionaltestsXmlFile(),
            str_replace(
                self::PLACEHOLDER_IN_PHPUNIT_FUNCTIONALTESTS_XML_FILE_DIRECTORY_ELEMENTS_WHITELIST,
                implode('', $replacement),
                file_get_contents($this->getPathPhpunitFunctionaltestsXmlFile())
            )
        );
    }

    /**
     * @return array
     */
    protected function getProjectSpecificExtensionFolderNames()
    {
        return array_diff(
            $this->getAllExtensionFolderNames(),
            $this->getExtensionNamesFromPackageNames(
                $this->getPackageNamesOfTypo3ExtensionsInstalledViaComposer()
            )
        );
    }

    /**
     * @return array
     */
    protected function getNotProjectSpecificExtensionFolderNames()
    {
        return $this->getExtensionNamesFromPackageNames(
            $this->getPackageNamesOfTypo3ExtensionsInstalledViaComposer()
        );
    }

    /**
     * @return void
     */
    protected function replacePathPlaceholdersInPhpcsfixerConfigurationFileCommaseparatedPaths()
    {
        $paths = [];

        if ($this->composerRootPackageIsProject()) {
            $paths[] = "'Build/WorkingDirectoryForTools/CopyOfProject'";
        } elseif ($this->composerRootPackageIsTypo3CmsExtension()) {
            $paths[] = "'.Build/Build/WorkingDirectoryForTools/CopyOfProject'";
        } else {
            foreach ($this->getProjectSpecificExtensionFolderNames() as $folderName) {
                $paths[] = sprintf(
                    "'Web/typo3conf/ext/%s'",
                    $folderName
                );
            }
        }

        $replacement = implode(',', $paths);

        file_put_contents(
            $this->getPathPhpcsfixerConfigurationFile(),
            str_replace(
                self::PLACEHOLDER_IN_PHPCSFIXER_CONFIGURATION_FILE_PATHS_COMMASEPARATED_PATHS,
                $replacement,
                file_get_contents($this->getPathPhpcsfixerConfigurationFile())
            )
        );
    }

    /**
     * @return string
     */
    protected function getBuildDirectoryPath()
    {
        if ($this->composerRootPackageIsProject()) {
            $result = './Build/';
        } elseif ($this->composerRootPackageIsTypo3CmsExtension()) {
            $result = './.Build/Build/';
        } else {
            $result = '../Build/';
        }

        return $result;
    }

    /**
     * @return string
     */
    protected function getWebDirectoryPath()
    {
        if ($this->composerRootPackageIsTypo3CmsExtension()) {
            $result = './.Build/Web/';
        } else {
            $result = './';
        }

        return $result;
    }
    /**
     * @return string
     */
    protected function getPathProjectRoot()
    {
        if ($this->composerRootPackageIsProject()) {
            $result = './';
        } elseif ($this->composerRootPackageIsTypo3CmsExtension()) {
            $result = './';
        } else {
            $result = '../';
        }

        return $result;
    }

    /**
     * @return string
     */
    protected function getPathEditorconfigSourceFile()
    {
        return $this->getBuildDirectoryPath() . 'In2codeBuildessentials/VariousFiles/.editorconfig';
    }

    /**
     * @return string
     */
    protected function getPathEditorconfigTargetFile()
    {
        return $this->getPathProjectRoot() . '.editorconfig';
    }

    /**
     * @return string
     */
    protected function getPathPhpcsfixerConfigurationFile()
    {
        return $this->getBuildDirectoryPath() . 'In2codeBuildessentials/PhpCsFixer/.php_cs';
    }

    /**
     * @return string
     */
    protected function getPathPhpunitUnittestsXmlFile()
    {
        return $this->getBuildDirectoryPath() . 'In2codeBuildessentials/PhpUnit/UnitTests.xml';
    }

    /**
     * @return string
     */
    protected function getPathPhpunitFunctionaltestsXmlFile()
    {
        return $this->getBuildDirectoryPath() . 'In2codeBuildessentials/PhpUnit/FunctionalTests.xml';
    }

    /**
     * @return string
     */
    protected function getPathPhpdoxXmlFile()
    {
        return $this->getBuildDirectoryPath() . 'In2codeBuildessentials/PhpDox/phpdox.xml';
    }

    /**
     * @return string
     */
    protected function getPathBuildfile()
    {
        return $this->getPathProjectRoot() . self::BUILDFILE_NAME;
    }

    /**
     * @return string
     */
    protected function getPathPhpdoxXmlFileWithPlaceholders()
    {
        return $this->getBuildDirectoryPath() . 'In2codeBuildessentials/PhpDox/phpdox_with_placeholders.xml';
    }

    /**
     * @return string
     */
    protected function getPathBuildfileWithPlaceholders()
    {
        return $this->getBuildDirectoryPath() . 'In2codeBuild/Resources/Private/Build/build_with_placeholders.xml';
    }

    /**
     * @return string
     */
    protected function getPathPhpunitUnittestsXmlFileWithPlaceholders()
    {
        return $this->getBuildDirectoryPath() . 'In2codeBuildessentials/PhpUnit/UnitTests_with_placeholders.xml';
    }

    /**
     * @return string
     */
    protected function getPathPhpunitFunctionaltestsXmlFileWithPlaceholders()
    {
        return $this->getBuildDirectoryPath() . 'In2codeBuildessentials/PhpUnit/FunctionalTests_with_placeholders.xml';
    }

    /**
     * @return string
     */
    protected function getPathPhpcsfixerConfigurationFileWithPlaceholders()
    {
        return $this->getBuildDirectoryPath() . 'In2codeBuildessentials/PhpCsFixer/.php_cs_with_placeholders';
    }

    /**
     * @return string
     */
    protected function getPathTypo3ExtensionsDirectory()
    {
        return $this->getWebDirectoryPath() . 'typo3conf/ext/';
    }

    /**
     * @return string
     */
    protected function getPathPackageJsonFileFallback()
    {
        return $this->getBuildDirectoryPath() . 'In2codeBuild/Resources/Private/Build/package.json';
    }

    /**
     * @return string
     */
    protected function getPathPackageJsonFile()
    {
        return $this->getBuildDirectoryPath() . 'package.json';
    }

    /**
     * @return bool
     */
    protected function composerRootPackageIsTypo3CmsExtension()
    {
        $result = false;

        $rootPackageType = $this->composerScriptEvent->getComposer()->getPackage()->getType();
        if ($rootPackageType === 'typo3-cms-extension') {
            $result = true;
        }

        return $result;
    }

    /**
     * @return bool
     */
    protected function composerRootPackageIsProject()
    {
        $result = false;

        $rootPackageType = $this->composerScriptEvent->getComposer()->getPackage()->getType();
        if ($rootPackageType === 'project') {
            $result = true;
        }

        return $result;
    }

    /**
     * @return bool
     */
    protected function weUseTheExternalTestingFrameworkProvidedByTheTypo3CoreTeam()
    {
        $result = false;

        $packages = $this->composerScriptEvent
            ->getComposer()
            ->getRepositoryManager()
            ->getLocalRepository()
            ->getPackages();

        $packagesOfInterest = array_filter($packages, function (\Composer\Package\BasePackage $package) {
            if (
                $package->getName() === 'typo3/cms'
                &&
                // Ignore e.g. AliasPackages
                is_a($package, \Composer\Package\CompletePackage::class)
                &&
                (
                    (
                        $package->getPrettyVersion() === 'dev-master'
                        &&
                        // On 2017-03-31 a breaking change was introduced in
                        // TYPO3 CMS: https://forge.typo3.org/issues/80606
                        $package->getReleaseDate() > new \DateTime('2017-03-31')
                    )
                    ||
                    Version::parse($package->getVersion())->getMajor() > 8
                    ||
                    (
                        Version::parse($package->getVersion())->getMajor() >= 8
                        &&
                        Version::parse($package->getVersion())->getMinor() >= 7
                    )
                )
            ) {
                return true;
            }
        });

        if (count($packagesOfInterest) > 0) {
            $result = true;
        }

        return $result;
    }

    /**
     * @return bool
     */
    protected function weUseTheInternalTestingFrameworkProvidedByTheTypo3CoreTeam()
    {
        $result = false;

        $packages = $this->composerScriptEvent
            ->getComposer()
            ->getRepositoryManager()
            ->getLocalRepository()
            ->getPackages();

        $packagesOfInterest = array_filter($packages, function (\Composer\Package\BasePackage $package) {
            if (
                $package->getName() === 'typo3/cms'
                &&
                // Ignore e.g. AliasPackages
                is_a($package, \Composer\Package\CompletePackage::class)
                &&
                (
                    (
                        $package->getPrettyVersion() === 'dev-master'
                        &&
                        // On 2016-12-22 and 2017-02-03 breaking changes were
                        // introduced in TYPO3 CMS:
                        // https://forge.typo3.org/issues/79025#note-3
                        // https://review.typo3.org/#/c/51449/
                        $package->getReleaseDate() > new \DateTime('2017-02-03')
                    )
                    ||
                    (
                        Version::parse($package->getVersion())->getMajor() >= 8
                        &&
                        Version::parse($package->getVersion())->getMinor() >= 6
                    )
                )
            ) {
                return true;
            }
        });

        if (count($packagesOfInterest) > 0) {
            $result = true;
        }

        return $result;
    }
}
