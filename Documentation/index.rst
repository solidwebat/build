=============
In2code/Build
=============

Breaking Changes in Version 2.0.0
=================================

A new Composer script "prepare" has to be defined in every project's root
composer.json file. Furthermore the "post-update-cmd" and "post-install-cmd" scripts
from now on do not share the same but have their own method to be called:

.. code-block:: json

	"scripts": {
		"post-update-cmd": "In2code\\Build\\Composer::postUpdate",
		"post-install-cmd": "In2code\\Build\\Composer::postInstall",
		"prepare": "In2code\\Build\\Composer::prepare"
	}

Important: do not forget to raise the version number of "In2code/Build" to "^2.0.0"
in the "require" object of the project's root composer.json file as well!

.. code-block:: json

	"require": {
		"in2code/build": "^2.0.0",
		"foo/bar:" "^1.0.0",
		"baz/bam:" "^1.0.0",
	}
