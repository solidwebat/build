<?php
namespace In2code\Build\Tests\PHPUnit;

use PHPUnit\Framework\TestCase;
use SebastianBergmann\PeekAndPoke\Proxy;

/**
 * @coversDefaultClass \In2code\Build\Composer
 * @covers ::<!public>
 */
class ComposerTest extends TestCase
{
    /**
     * @test
     * @covers ::__construct
     * @group unit
     * @return void
     */
    public function setsTheComposerEventAttributeOnCreation()
    {
        // Arrange + Act
        $expectedComposerScriptEventDummy = $this->getMock(\Composer\Script\Event::class, [], [], '', false);
        $subject = new \In2code\Build\Composer($expectedComposerScriptEventDummy);

        // Assert
        $subjectProxy = new Proxy($subject);
        $actualComposerScriptEventDummy = $subjectProxy->composerScriptEvent;
        $this->assertSame($expectedComposerScriptEventDummy, $actualComposerScriptEventDummy);
    }
}
